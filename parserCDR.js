var conectorSunat=require("./soapClient");
var jsZip=require("jszip");
var parseString=require("xml2js").parseString;
var moment=require("moment");


function ParserCDR() {}


ParserCDR.prototype.parsearCDR=function(val, cb){
     
    conectorSunat.consultarCDR(val, function(err, res){
        if(err){
            cb(err, null);
        }else{

           var cdrData =  {}; 
           var xmlBase64 = res["statusCdr"]["content"] || null;
            
           cdrData.respuestaSoap = res["statusCdr"]["statusMessage"] || null;
           
           if (xmlBase64 != null) {
           
                   var zip = new jsZip();
                   zip.loadAsync(xmlBase64, {base64:true}).then(function (file){
                       let arr = Object.values(file.files)    
                       //console.log(arr)                     
                       let arr2 = Object.values(arr[0])
                       // XML sin transformar
                       //console.log(arr2)                        
                           zip.file(arr2[0]).async("string")
                           .then(function (content) {

                               cdrData.xmlBase64 = xmlBase64; //Devuelve el xml del CDR
                                               
                               parseString(content, 
                                   {
                                    explicitArray: true,
                                    normalize: true,
                                    normalizeTags: false
                                   },                            
                                
                                function (err, xmlObj) {
                                   if (err) {
                                       cb(err, null);
                                   }    
                                   try {
                                       var root = xmlObj["ar:ApplicationResponse"] || xmlObj["ApplicationResponse"];
                                       
                                       cdrData.idRespuesta = root["cbc:ID"][0]; //Número único asignado por SUNAT para identificar el proceso de recepción
                                       // La fechaEmision es la fecha en la cual SUNAT aceptó/rechazó el comprobante
                                       cdrData.descripcion =  "VALIDATOR: " + root["cac:DocumentResponse"][0]["cac:Response"][0]["cbc:Description"][0];
                                       cdrData.codigo = root["cac:DocumentResponse"][0]["cac:Response"][0]["cbc:ResponseCode"][0]; //Información sobre la respuesta - Respuesta al documento recibido-  Código de la respuesta al documento electrónico procesado
                                       cdrData.fechaEmision = root["cbc:IssueDate"] + " " + root["cbc:IssueTime"];//Fecha y Hora de recepción del documento procesado
                                       cdrData.fechaEmisionFormat = moment(cdrData.fechaEmision).format("YYYY-MM-DD");
                                       var serieNumero = root["cac:DocumentResponse"][0]["cac:Response"][0]["cbc:ReferenceID"][0].split("-");
                                       
                                       
                                       if(serieNumero[2]){
                                           cdrData.serie = serieNumero[2];
                                           cdrData.numero = serieNumero[3];
                                       } else {
                                           cdrData.serie = serieNumero[0];
                                           cdrData.numero = serieNumero[1];
                                       }
                                                                   
                                      
                                       //console.log('notes:', notes)
                                       cdrData.notas = "";

                                       var notes = root["cbc:Note"];

                                       if (notes) {
                                           var notas=[];
                                           notes.forEach(function(note){
                                             var text = note.replace(/["']/g, " ");
                                             notas.push(text);
                                           });
                                        
                                        
                                        cdrData.notas = notas;
                                       }
                                       cb(null, cdrData);
                                   } catch (e) {
                                       cb(e, null);
                                   } });

                       });
                       
                   })  

            } else {
                cdrData["respuestaSoap"] = " *********** CONSTANCIA NO EXISTE *********** "; 
                cb(null, cdrData);
            }
        }
    });
};

module.exports = new ParserCDR();