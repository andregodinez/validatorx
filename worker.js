"use strict";
var CronJob=require("cron").CronJob;
var Async=require("async");
var Mining=require("./queryDb");
var ParserCDR=require("./parserCDR");
var Moment=require("moment");


function worker(){ }

worker.prototype.workerStart=function() {
   var job= new CronJob({
        cronTime: "*/5 * * * * *",
        onTick: function(){
            Async.parallel([

// **************************************************************************** COMPROBANTE *****************************************************************************
        function(){
             // Tipos de Comprobantes
             //  01: Factura
             //  07: NotaCredito
             //  08: NotaDebito
             //  09: GuiaRemision
            var tipoComprobante="07"; 
            var idComprobantevalor=""

            setTimeout(function() {
                Mining.query(tipoComprobante, function(err,data){
                    if(err){
                        
                    console.log(err);
                    }else{       
                        
                        switch(tipoComprobante){
                            case "01":  idComprobantevalor = data.idFactura;
                                        break;
                            case "07":  idComprobantevalor = data.idNotaCredito;
                                        break;
                            // case "08": idComprobantevalor = data.idNotaDebito;
                            //             break;
                            case "09": idComprobantevalor = data.idGuiaRemitente;
                                        break;
                        }
                        
                        if(idComprobantevalor){
                            data.tipoDoc=tipoComprobante;                          
                            if(data.serie && data.numero){
                                var val={
                                    usuario: "20601156530ERV1NAGU",
                                    password: "aguirreocana",
                                    rucEmisor: data.idEmisor,
                                    tipoComprobante: data.tipoDoc,
                                    serieComprobante: data.serie,
                                    numeroComprobante: data.numero                                
                                };

                                ParserCDR.parsearCDR(val, function(err, cdr){
                                    if(err){
                                        console.log(1)
                                        console.log(err);
                                    }else{
                                        console.log(cdr)
                                        var fechaEmisionValid = Moment(data.fechaEmision).format("YYYY-MM-DD");
                                        
                                        if(cdr.respuestaSoap === "La constancia existe" && cdr.codigo=="0"){
                                            cdr.tipo=data.tipoDoc;
                                            cdr.idEmisor=data.idEmisor;
                                            
                                            Mining.createConstancia(cdr, function(err, respConstancia){
                                                if(err){
                                                    console.log(err);
                                                }else{
                                                   // console.log(cdr.)
                                                    var idConstancia = respConstancia;
                                                    
                                                    Mining.update(tipoComprobante, idComprobantevalor, 1, idConstancia, function(err, data){
                                                        
                                                        if(err){
                                                            console.log(err);
                                                        }else{
                                                            console.log(data);
                                                        }
                                                    })
                                                //********************* VISTAS FECHAS NO IMPLEMENTADO */
                                                /*
                                                }else{ console.log(' ******** REVISAR FECHAS ******** ')
                                                */
                                                }
                                            }) 
                                        }
                                    }
                                });
                            }
                        } else { 
                                console.log(" ************** RARO ************** ")
                        }
                    }
                });
            });
        }

// ************************************************************************** FIN COMPROBANTE *****************************************************************************

            ]);
        },
        start: false,
        timeZone: "America/Los_Angeles"
    });
    job.start();
};

module.exports=new worker();

