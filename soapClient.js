var soap = require("soap");
var url = "https://e-factura.sunat.gob.pe/ol-it-wsconscpegem/billConsultService?wsdl";

function Comppagoconector(){
}

Comppagoconector.prototype.consultarCDR = function(val, cb){
	var args = { 
		attributes:{
			'xmlns:m':"http://service.sunat.gob.pe"
		},
		rucComprobante: val['rucEmisor'],
		tipoComprobante: val['tipoComprobante'],
		serieComprobante: val['serieComprobante'],
		numeroComprobante: val['numeroComprobante']
		};
		
	soap.createClient(url, function(err, client){
		var wsSecurity = new soap.WSSecurity(val['usuario'], val['password'])

		if (err) {
			cb(err);
		  } else {
			client.setSecurity(wsSecurity);
			if (err) {
			  cb(err);
			} else {
			  client.getStatusCdr(args, function(err, result) {
				if (err) {
				  cb(err, null);
				} else {
				  cb(null, result);
				}
			  });
			}
		  }
		});
};
	  
module.exports = new Comppagoconector();
	  