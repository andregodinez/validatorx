"use strict";
var mysql=require("mysql");
var dbconfig=require("./config.json");

var connection=mysql.createConnection(dbconfig);

var connectDb=function(){
    connection.connect(function(err){
        if(err){
            console.log(err);
        } else{
            console.log(" ************** CONECTADO A LA DB ************** ")
        }
    });
};

connectDb("error", function(err){
    console.log("Db Error", err);
    console.log("queryDb dberror: "+err.code);

    if(err.code === "PROTOCOL_CONNECTION_LOST"){
        connectDb();
    }else{
        throw err;
    }
});

function getDataInvoice(tipo, callback){
    try{
        //console.log(tipo);
        if(tipo === "01"){ var query=`SELECT * FROM clientes_factura WHERE emitido=0 limit 1`}
        if(tipo === "07"){ var query=`SELECT * FROM clientes_notacredito WHERE emitido=0 and tipoComprobanteAfectado = '01' limit 1`;}
        if(tipo === "08"){ var query=`SELECT * FROM clientes_notadebito WHERE emitido=0 and tipoComprobanteAfectado = '01' limit 1`;}
        if(tipo === "09"){ var query=`SELECT * FROM clientes_guiaremitente WHERE emitido=0 limit 1`;}

        console.log(query);

        connection.query(query, function(err, rows){
            if(err){
                console.log(`Error al consulta el comprobante: ${err} `);
                return callback(" *********** NO EXISTE EL COMPROBANTE *********** ", null);
            }else{
                var guiaremitente=rows[0];
                if(guiaremitente){
                    return callback(null, guiaremitente);
                }
            }
        });
       } catch(e){
            console.log(e);
            return callback("*********** ERROR EN LA CONSULTA *********** ", null);
       }
}
   
function createConst(cdr, callback){
    try{
        var query="INSERT INTO clientes_constancia (idRespuesta, serie, tipo, numero, codigo, notas, descripcion, fechaEmision, idEmisor, xml) VALUES "
                   +'("'+cdr.idRespuesta +'", "'+cdr.serie+ '", "'+cdr.tipo+'", "'+cdr.numero+'", "'+cdr.codigo+'", "'+cdr.notas
                   +'", "'+cdr.descripcion+'", "'+cdr.fechaEmision+'", "'+cdr.idEmisor+'", "'+cdr.xmlBase64+'")';    

        connection.query(query, function(err, rows, fields){
            if(err){
                return callback(err, null);
            } else{
                console.log(rows.insertId)
                return callback(null, rows.insertId);
            }
        });     
    } catch(e){
        return callback(" *********** ERROR ACTUALIZANDO CONSTANCIA *********** ", null);
    }
}

function updateInvoice(tipo, id, value, idConstancia, callback){
    try{
        var table=getTableName(tipo);
        var typeId="";
        if(idConstancia != null){
            var query="UPDATE "+table.tablename+" SET emitido = "+value+", idConstancia= "+idConstancia+" WHERE "+table.idname+" = "+id;
        }
        console.log(query);
        connection.query(query, function(err, rows, fields){
            if(err){
                return callback(err, null);
            }else{
                return callback(null, " ************ CONSTANCIA ACTUALIZADA ************ ");
            }
        });
    }catch(e){
        return callback(" ************ ERROR EN LA CONSULTA ************ ", null);
    }
}

// *********************************** TRAER  DE LA TABLA *********************************************
function getTableName(typeInvoice){
    if(typeInvoice){
        switch(typeInvoice){
            case "01": return { tablename: "clientes_factura", idname: "idFactura" };
            case "07": return { tablename: "clientes_notacredito", idname: "idNotaCredito" };
            case "08": return { tablename: "clientes_notadebito", idname: "idNotaDebito" };
            case "09": return { tablename: "clientes_guiaremitente", idname: "idGuiaRemitente" };
            default:
                return "*********** ERROR *********** ";
        }
    } else{
        return "error";
    }
}

var Mining=function() {};

Mining.prototype.query=function(tipo, callback){
    getDataInvoice(tipo, function(err, data){
        if(err){
            return callback(err, null);
        } else{
            return callback(null, data);
        }
    });
};

Mining.prototype.createConstancia=function(cdr, callback){
    createConst(cdr, function(err, respConstancia){
        if(err){
            return callback(err, null);          
        }else{
            return callback(null, respConstancia);   
        }
    });
};

Mining.prototype.update=function(tipo, id, value, idConstancia, callback){
    updateInvoice(tipo, id, value, idConstancia, function(err, data){
        if(err){
            return callback(err,null);
        }else{
            var response=data;
            return callback(null,response);
        }
    });
};


module.exports=new Mining();